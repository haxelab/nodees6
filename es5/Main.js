/**
 * Created by freem on 31.05.2015.
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Config = require('./Config');
var net = require('net');

'use strict';

var Main = (function () {
    function Main() {
        _classCallCheck(this, Main);

        this.clients = [];
        this.config = new Config(this);

        this.log(this.clients);
        this.log(this);

        this.server = net.createServer(this.onConnection);
        this.server.listen(this.config.port, this.config.ip);
    }

    _createClass(Main, [{
        key: 'onConnection',
        value: function onConnection(socket) {
            console.log(this);
            socket.name = socket.remoteAddress + ':' + socket.remotePort;
            this.clients.push(socket);

            this.wellcome(socket);

            socket.on('data', this.onData);

            // Remove the client from the list when it leaves
            //socket.on('end', function ()
            //{
            //    clients.splice(clients.indexOf(socket), 1);
            //    broadcast(socket.name + " left the chat.\n");
            //});
        }
    }, {
        key: 'wellcome',
        value: function wellcome(socket) {
            socket.write('Welcome ' + socket.name + '\n');
            this.broadcast(socket.name + ' joined the chat\n', socket);
        }
    }, {
        key: 'onData',
        value: function onData(data) {
            this.broadcast(socket.name + '> ' + data, socket);
        }
    }, {
        key: 'broadcast',
        value: function broadcast(message, sender) {
            this.clients.forEach(function (client) {
                if (client === sender) return;

                client.write(message);
            });
            this.log(message);
        }
    }, {
        key: 'log',
        value: function log(message) {
            console.log(message);
        }
    }, {
        key: 'warn',
        value: function warn(message) {
            console.warn('!> ' + message);
        }
    }]);

    return Main;
})();

exports['default'] = Main;
module.exports = exports['default'];
//# sourceMappingURL=Main.js.map