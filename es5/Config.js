/**
 * Created by freem on 31.05.2015.
 */

"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Config = (function () {
    function Config(main) {
        _classCallCheck(this, Config);

        this.main = main;
        this.setupVariables();
    }

    _createClass(Config, [{
        key: "setupVariables",
        value: function setupVariables() {
            this.ip = process.env.OPENSHIFT_NODEJS_IP;
            this.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

            if (typeof this.ip === "undefined") {
                this.ip = "127.0.0.1";
                this.main.warn("No OPENSHIFT_NODEJS_IP var");
            }
            this.main.log("host: " + this.ip);
            this.main.log("port: " + this.port);
        }
    }]);

    return Config;
})();

exports["default"] = Config;
module.exports = exports["default"];
//# sourceMappingURL=Config.js.map