/**
 * Created by freem on 31.05.2015.
 */

"use strict";
class Config {

    constructor(main)
    {
        this.main = main
        this.setupVariables();
    }

    setupVariables()
    {
        this.ip = process.env.OPENSHIFT_NODEJS_IP;
        this.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

        if (typeof this.ip === "undefined")
        {
            this.ip = "127.0.0.1";
            this.main.warn('No OPENSHIFT_NODEJS_IP var');
        }
        this.main.log('host: ' + this.ip);
        this.main.log('port: ' + this.port);
    }

}

export default Config;